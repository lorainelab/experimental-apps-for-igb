package org.lorainelab.igb.ncbiprimer;

import apollo.datamodel.Sequence;
import apollo.datamodel.StrandedFeatureSet;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Notes from aloraine: Moved this class from main to test. Looks like it
 * pre-dates migration to maven from ant and previously was not set up for
 * formal, regular testing. I've updated it a bit - added imports and @Test
 * annotations. This looks like it will be very useful for figuring out why the
 * call to NCBI server no longer works.
 *
 * Note to the next person who works on this: Please use logging and NOT
 * System.out.println statements if you need to see what things are getting
 * printed.
 *
 * The entire Experimental Apps project is set up so that the SLF4J logging
 * framework API implementation is coming from the framework - e.g., IGB itself.
 * However, during the test phase of the default build life cycle for this
 * project, IGB is not available. So I added SLFJ API implementation artifacts
 * to the parent POM. I used the same implementations that are used in IGB 9.1.0
 * and assigned scope of "test." This ensures that any logging statements in the
 * target code or in the testing code can run.
 *
 * If you run the commented test below, you will find that the test gives an
 * error because the call to the NCBI server is not working properly, possibly
 * because the NCBI server has changed since this was first implemented? I don't
 * know why it is failing. The next developer who works on this should
 * investigate.
 *
 */
public class RemotePrimerBlastNCBITest {

    private static final Logger logger = LoggerFactory.getLogger(RemotePrimerBlastNCBITest.class);
    String PRIMER_BLAST_URL = "https://www.ncbi.nlm.nih.gov/tools/primer-blast/primertool.cgi";
    String id = "test";
    String residues = "TTCAGAGTTAAGGAACTGTATTAATCGTGTAATTTCTGCTACGAATCCGCCCATTTGTATTTAGTGTGCT"
            + "TCATGTACTATCATTCTCCTAAATTGGTTTTTTGAGTGGTTATTAATCTGCTTTGCAGTCAGGAGGAATC"
            + "ATCTAAGTCAGTATGGACCAGAAGAAGAGGAAGTTGGAAGATCCACCAGCCACGAATGCGAAAGAAGACA"
            + "ACACTGCTGTAGTTGGGGGACCAACAGTGTCTGTTCCTTCTTTCGTGATCACTAAGGAGGATGTCAAGAA"
            + "GCTTTTGGAGTCATTTACGAAGGATCGATTGATGGATTTATTGGAAAATGCTGCTCTTGTTCATACAGGT"
            + "TATTCCTCGTCGAGTTATCTATCGTCCTAAAGCTCATTTTGGATGTGGTTTTTCAACGCGAGAGATACAG"
            + "TGATGGGTTTATATATTTTCATGTACTGTTCAACTTATTCACATGTTCTCGATGGGTGTCGCTGTTTGAC"
            + "TTTTGCAGATGTTCTTCAGGAAATTCGCAAGGTGGCTGACAAAGTTCCTGGTCATCGGAAGATTTTTGTA"
            + "AGGGGCCTTGGTTGGGACTCAACGACAGAAACCTTGAAGACAGTATTCTCACAATTTGGAGAAGTGGAGG"
            + "AGGGGGCGGTTGTTATGGACAAGGGTACTGGAAAGAGTAGAGGCTTTGGATTTGTGACATTCATACACAT"
            + "GGACGGTGCTCAACGATCACTGAAGGAACCTAGCAAGCGGATTGATGGACGGATGACAGATTGTCAGTTG"
            + "GCATCAACTGGGGCGTTGCAAATAAATCCTAATCAAGAAGTTTCAACGAGAAAAATTTACGTGGGCAACA"
            + "TATCCCTTGATCTTTCTGCGGACAGGCTGCTGGCTTTTTTCGCAGGATATGGAGAAATTGGAGAGGGTCC"
            + "CTTAGGATTCGACAAATTCACTGGACGCTCACGAGGTTTCGCTTTGTTTATTTACAAGACAGTTGAAGCT"
            + "ACAAAGAGGGCATTGTTGGAGCCAATCAAGACTTTAGATGGTATTCAAATGTACTGTAAGCTTGCCTCTG"
            + "AGAATCAGAAACAGGGGCAGGGGAGGCAAGGCCCTCCCGGTAAAAGCGACAATGATTTAATACTAGGCAG"
            + "GCAGCAGGTAATCTACATCAATCATCTTGTCCATCTGAAAATGCGTTTATTCTTATGGTCGGGGACCAGT"
            + "TTGTATTTTAAAGCTGATGTGGTGTTCATTTGATCACATGTTTTTACGAAGTGGTACTGAGAGTAGTTGT"
            + "TGTGAGAACCTTCTGCTGGCTTTTCTTGTGTGCAGATATCAACTCCTCCGTCCCTCTCAAACGAACAAAA"
            + "TATGTCGTACGGATCCATGAATACTGGTTCATTAACTACTGGAATTCCGCTCAACCAGGGCGTGAATCAA"
            + "GGAATGAGTCAAGGAAGTCACATGGGTCTCAATAATCATAATCAAGGTCTTAGCAACCTGAAGAGCACAT"
            + "TGAACTCGCCCCTCAACTATACATTGAATTCATCATTATCTCCTCTAGGACAGTTAACGAATTCATCGTT"
            + "AAATGGTTCTTTTAACTCCGGTCACCCACAAACATCCCTTGGAATGGGATCTTACAGTTCACAATTGGCT"
            + "GTGTCCCAATATGGAGGTCAACCAACTGGCCTTGGTGGGCAACCTACATATTCTTCGATGAATGGGGTTC"
            + "TTTATTGCTCGGCAGCCAACTCAGCAGCCTCGCAGCAGGTATTTAAAACGAATTTTATATTGCGCATGAC"
            + "GATTTAGTGTAGGTCCTGTAATTTGCATGCTTAACTGTCTATTTTAATTAATTACATTACATCTAATGAG"
            + "TGAGGCCGTTCGTATTATTCGGGATAACTGTGTTGATCTTGGCGGACCTGCAGTTTGTACCGAAGTACTT"
            + "GACGTTGCATTCGCCTAATACTATGTCCAACACATGTATATTTAAAATCTGACTTCTCTCATCTACTGTA"
            + "TATTTCTCAGCAAGCAGCGCTGCAGGTAGCTGGTGGTCATTACGGATATGGGTCATATCAAAGTCCTCAA"
            + "CTTCCAACATCTTCTGCTCCTCGCGCCCCTCCCGTAGGAAATATGTACATGCCTTCCAACCATAACCTAA"
            + "AAGGTAAGCATTGGTGTCTTCACTTTCTTGCTCTTTAAGGTGCGCTATTCTTTTACTGACCAATTGTGAG"
            + "ACCTCCTAATTAAATATGCTCGAATGATTGTCCTCTTTTCGTTCAAAGTTGCGAGTTTGGTTCACGTCTG"
            + "AGAGAGGCATCGGAGTTTTGAATGAATATTAAATCTTGGATGTTTTATTTTGGATGTTGAATACAAATTC"
            + "AGCAATAACAATTTTTTTGTCTGTGATGTTTCATGTCTGGATTATCTACATGTCTGTGATCACTGTTCAG"
            + "TGTATACAAGAGTTTCAGTTGTTTTCAATTAGGATGATTATGTTTGTGCTACATTGGGCAACAGAGGTTA"
            + "TTTGTGAGAAGGGAGATTTAAGTACTAAACATTGTGCTTACTGTAAATGTTCTGTTGCGGATTTTGTTCT"
            + "TTTGACTTTTTCTTTTTTTCTTTTTTTCTTTTCTCCAGTTCTTTGCAAATTGTGATAGTTTAACCTTTTT"
            + "ATCTTAATAATGTTGCAAGCTTGTTGGTCTAGTTTCTTTGAGGAATTCAAGTTTCGAACGGGATATTACC"
            + "ACAGTTTGATAGCAAATACAAAAAACAAGAGTTGAAATTAGTGGTGAAAAAGTTTGGTCATTTCATAACC"
            + "GATGCCCATAATTTGAATCTCTGTGACTCTGAAAATCTCTAATATTTATCGTTTTTATCCATCAACTCTA"
            + "GCGTACGCCATGCACTACTTATTGTTCTAGTAATTGACGGATCTCTGTTGACAATGTTGGTGGACATGGG"
            + "ATTACCATGAGATAAGTTCCAGGCCCGATCGACCAGGTTTCACAAAGAAGCTTTAAAGATTATTTTTTCT"
            + "TATCAAATTTAATGTATAAAGATCCAAAGTGGTTTATTGCCATGTTTTCAGTCAAGTAATTTCATGACTG"
            + "AAAAGTTATGGAGACTAATTATTCCTTTTAATTGTA";

    @Test
    public void testRunAnalysis_URL() throws Exception {
        logger.info("Test for NCBI Primer URL");
        URL resURL = new URL(PRIMER_BLAST_URL);
        if (resURL.toString().contains("https")) {
            if (((HttpsURLConnection) resURL.openConnection()).getResponseCode() != HttpsURLConnection.HTTP_OK) {
                fail("Invalid URL");
            }
        } else if (resURL.toString().contains("http")) {
            if (((HttpURLConnection) resURL.openConnection()).getResponseCode() != HttpURLConnection.HTTP_OK) {
                fail("Invalid URL");
            }
        }
        assertNotNull(PRIMER_BLAST_URL);
    }

    @Test
    public void testRunAnalysis_refseq_rna() throws Exception {
        String resultURL;
        StrandedFeatureSet cs = new StrandedFeatureSet();
        Sequence seq = new Sequence(id, residues);
        cs.setRefSequence(seq);
        NCBIPrimerBlastModel opts = new NCBIPrimerBlastModel();

        opts.setDefaultOptions();

        logger.info("Test for database : refseq_rna");
        resultURL = runTests(opts, cs, seq, NCBIPrimerBlastModel.Database.refseq_rna);
        testPassOrFail(resultURL);

    }

    @Test
    public void testRunAnalysis_genome_selected_species() throws Exception {
        String resultURL;
        StrandedFeatureSet cs = new StrandedFeatureSet();
        Sequence seq = new Sequence(id, residues);
        cs.setRefSequence(seq);
        NCBIPrimerBlastModel opts = new NCBIPrimerBlastModel();

        opts.setDefaultOptions();

        logger.info("Test for database : genome_selected_species");
        resultURL = runTests(opts, cs, seq, NCBIPrimerBlastModel.Database.genome_selected_species);
        testPassOrFail(resultURL);
    }

    @Test
    public void testRunAnalysis_genome_selected_species2() throws Exception {
        String resultURL;
        StrandedFeatureSet cs = new StrandedFeatureSet();
        Sequence seq = new Sequence(id, residues);
        cs.setRefSequence(seq);
        NCBIPrimerBlastModel opts = new NCBIPrimerBlastModel();
        opts.setDefaultOptions();
        opts.setOrganism("Arabidopsis thaliana");
        logger.info("Test for database : genome_selected_species, Organism : Arabidopsis thaliana  ");       

        resultURL = runTests(opts, cs, seq, NCBIPrimerBlastModel.Database.genome_selected_species);
        assertTrue(resultURL.toLowerCase().contains("error"));
    }

    @Test
    public void testRunAnalysis_nt() throws Exception {
        String resultURL;
        StrandedFeatureSet cs = new StrandedFeatureSet();
        Sequence seq = new Sequence(id, residues);
        cs.setRefSequence(seq);
        NCBIPrimerBlastModel opts = new NCBIPrimerBlastModel();

        opts.setDefaultOptions();

        logger.info("Test for database : nt");
        resultURL = runTests(opts, cs, seq, NCBIPrimerBlastModel.Database.nt);
        testPassOrFail(resultURL);

    }

    @Test
    public void testRunAnalysis_refseq_mrna() throws Exception {
        String resultURL;
        StrandedFeatureSet cs = new StrandedFeatureSet();
        Sequence seq = new Sequence(id, residues);
        cs.setRefSequence(seq);
        NCBIPrimerBlastModel opts = new NCBIPrimerBlastModel();

        opts.setDefaultOptions();

        logger.info("Test for database : refseq_mrna");
        resultURL = runTests(opts, cs, seq, NCBIPrimerBlastModel.Database.refseq_mrna);
        testPassOrFail(resultURL);

    }

    @Test
    public void testRunAnalysis_refseq_representative_genomes() throws Exception {
        String resultURL;
        StrandedFeatureSet cs = new StrandedFeatureSet();
        Sequence seq = new Sequence(id, residues);
        cs.setRefSequence(seq);
        NCBIPrimerBlastModel opts = new NCBIPrimerBlastModel();

        opts.setDefaultOptions();

        logger.info("Test for database : refseq_representative_genomes");
        resultURL = runTests(opts, cs, seq, NCBIPrimerBlastModel.Database.refseq_representative_genomes);
        testPassOrFail(resultURL);

    }

    public String runTests(NCBIPrimerBlastModel opts, StrandedFeatureSet cs, Sequence seq, NCBIPrimerBlastModel.Database db) throws Exception {
        opts.setPrimerSpecificityDatabase(db);
        RemotePrimerBlastNCBI primer = new RemotePrimerBlastNCBI(opts);
        return primer.runAnalysis(cs, seq);
    }

    public void testPassOrFail(String resultURL) {
        if (!resultURL.toLowerCase().contains("error")) {
            assertNotNull(resultURL);
        } else {
            fail(resultURL);
        }

    }

}
