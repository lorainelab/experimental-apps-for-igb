<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>org.lorainelab.igb</groupId>
    <artifactId>experimental-apps</artifactId>
    <version>0.0.1</version>
    <packaging>pom</packaging>
    <name>Experimental Apps for IGB</name>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <local.appstore>${basedir}/../obr/bundles</local.appstore>
        <!-- keep these in sync with https://bitbucket.org/lorainelab/integrated-genome-browser -->
        <igb.platform.version>9.1.0</igb.platform.version>
        <genovizVersion>1.1.8</genovizVersion>
        <htsjdk-igb-Version>2.16.3</htsjdk-igb-Version>
        <commonsLangVersion>3.4</commonsLangVersion>
        <felixMainVersion>5.2.0</felixMainVersion>
        <maven.bundle.plugin.version>3.0.0</maven.bundle.plugin.version>
        <bindlib-version>2.4.0</bindlib-version>
        <bundle-markdown-encoder.version>1.0.0</bundle-markdown-encoder.version>
        <maven-dependency-plugin.version>3.1.1</maven-dependency-plugin.version>
        <slf4j-api.version>1.7.7</slf4j-api.version>
        <logback-core.version>1.1.2</logback-core.version>
        <logback-classic.version>1.1.2</logback-classic.version>
        <junitVersion>4.8.2</junitVersion>
    </properties>
    <repositories>
        <repository>
            <id>maven-releases</id>
            <name>maven-releases</name>
            <url>https://nexus.bioviz.org/repository/maven-releases</url>
        </repository>
        <!-- might not need repo3 anymore -->
        <repository>
            <id>repo3</id>
            <url>https://maven.bioviz.org/repo3</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>maven-releases</id>
            <url>https://nexus.bioviz.org/repository/maven-releases</url>
        </pluginRepository>
    </pluginRepositories>
    <modules>
        <module>FindAnnotations</module>
        <module>NCBIPrimer</module>
        <module>OverlapAnnotationOperator</module>
        <module>ParentOperators</module>
        <module>SampleSelection</module>
        <module>SearchModeSymmetryFilter</module>
        <module>TallyHandler</module>
        <module>TranscriptIsoform</module>
        <module>obr</module> <!-- builds OBR index file for all the Apps -->
    </modules>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>igb</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>igb-services</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>org.lorainelab.igb.menu.api</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>genometry</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>igbSwingExt</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>affymetrix-common</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>igb-genoviz-extensions</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>org.lorainelab.igb</groupId>
                <artifactId>apollo-library</artifactId>
                <version>${igb.platform.version}</version>
            </dependency>
            <dependency>
                <groupId>com.affymetrix</groupId>
                <artifactId>genoviz</artifactId>
                <version>${genovizVersion}</version>
            </dependency>
            <dependency>
                <groupId>htsjdk-igb</groupId>
                <artifactId>htsjdk-igb</artifactId>
                <version>${htsjdk-igb-Version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commonsLangVersion}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.felix</groupId>
                <artifactId>org.apache.felix.main</artifactId>
                <version>${felixMainVersion}</version>
            </dependency>
            <dependency>
                <groupId>biz.aQute.bnd</groupId>
                <artifactId>bndlib</artifactId>
                <version>${bindlib-version}</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j-api.version}</version>
            </dependency>
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junitVersion}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    <!-- all the Apps need these -->
    <dependencies>
        <dependency>
            <groupId>org.apache.felix</groupId>
            <artifactId>org.apache.felix.main</artifactId>
        </dependency>
        <dependency>
            <groupId>biz.aQute.bnd</groupId>
            <artifactId>bndlib</artifactId>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId> <!-- SLF4J API -->
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>${logback-core.version}</version>
            <scope>test
            </scope> <!-- SLF4J implementation; needed only during test phase because framework provides this to Apps -->
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback-classic.version}</version>
            <scope>test
            </scope> <!-- SLF4J implementation; needed only during test phase because framework provides this to Apps -->
        </dependency>
    </dependencies>
    <!--
        The top-level POM builds nothing, so it inludes no plugins, just
        plugin configurations defined in the pluginManagement section.
        This lets us avoid repeating the below code in every
        module.
        If you need specialized plugin configurations in the future, you
        can override the default behaviors inherited from this parent POM.
        See the obr module for an example.
        Important: To override a behavior, the execution "id" must match. 
    -->
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.lorainelab</groupId>
                    <artifactId>bundle-markdown-encoder</artifactId>
                    <version>${bundle-markdown-encoder.version}</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>encodeMarkdown</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>${maven.bundle.plugin.version}</version>
                    <extensions>true</extensions>
                    <executions>
                        <execution>
                            <id>Build OBR index</id>
                            <goals>
                                <goal>index</goal>
                            </goals>
                            <phase>package</phase>
                            <configuration>
                                <!-- configured to build it for one App -->
                                <obrRepository>${project.build.directory}</obrRepository>
                                <mavenRepository>${project.build.directory}</mavenRepository>
                            </configuration>
                        </execution>
                        <execution>
                            <id>Include OBR index and resources in the bundle</id>
                            <goals>
                                <goal>bundle</goal>
                            </goals>
                            <configuration>
                                <instructions>
                                    <Include-Resource>${project.build.directory}/repository.xml,{maven-resources}
                                    </Include-Resource>
                                </instructions>
                            </configuration>
                        </execution>
                    </executions>
                    <configuration>
                        <instructions>
                            <Bundle-Name>${project.name}</Bundle-Name>
                            <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
                            <Import-Package>*</Import-Package> <!-- Let bnd figure out what needs to be imported -->
                            <Export-Package/> <!-- Apps for IGB should export nothing  -->
                            <!--
                                 Requires maven-bundle-plugin versions less than 4. The next person who works on this
                                 should investigate upgrading to an newer version of BND and ensure that these Apps can
                                 still run in the platform.
                            -->
                            <Service-Component>*</Service-Component>
                        </instructions>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>
